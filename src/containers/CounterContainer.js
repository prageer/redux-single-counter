import Counter from '../components/Counter';
import * as actions from '../actions';
import { connect } from 'react-redux';
import { getRandomColor } from '../utils';

// map state values of store to props
const mapStateToProps = (state) => ({
    color: state.colorData.color,
    number: state.numberData.number
});

/* 
	make action using action creator
	make func that dispatch the action
	map the func to props
*/

const mapDispatchToProps = (dispatch) => ({
    onIncrement: () => dispatch(actions.increment()),
    onDecrement: () => dispatch(actions.decrement()),
    onSetColor: () => {
        const color = getRandomColor();
        dispatch(actions.setColor(color));
    }
});


// Container Component of Counter component
// Bind Counter component with data layer

const CounterContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Counter);

export default CounterContainer;
